var PROJECT = PROJECT || {};

PROJECT.Setup = function(container, opts){
	opts = opts || {};

	var sphereMesh;

	var camera, scene, renderer, w, h;
	var atmosphere, point;

	var floor, floorMirrorCamera;

	var pointLight, ambientLight;

	var overRenderer;

	var mouseClick;
	var clickDetails;

	var curZoomSpeed = 0;
	var zoomSpeed = 50;

	var mouse = { x: 0, y: 0 }, mouseOnDown = { x: 0, y: 0 };
	var rotation = { x: 0, y: 0 },
		target = { x: Math.PI*3/2, y: Math.PI / 6.0 },
		targetOnDown = { x: 0, y: 0 };

	var distance = 5, distanceTarget = 5;
	var padding = 40;
	var PI_HALF = Math.PI / 2;

	function init(){

	    /*
	    *	World Object
	    */
		var geometry = new THREE.SphereGeometry(0.5, 32, 32);
		var material = new THREE.MeshPhongMaterial();
		material.map = THREE.ImageUtils.loadTexture('images/243.jpg');
		sphereMesh = new THREE.Mesh(geometry, material);
		sphereMesh.rotation.y = Math.PI;

		/*
		*	Reflective Floor
		*/
		var floorGeometry = new THREE.CubeGeometry( 100, 100, 10, 1, 1, 1 );
		floorMirrorCamera = new THREE.CubeCamera( 0.1, 5000, 512 );
		var floorMirrorMaterial = new THREE.MeshBasicMaterial( { envMap: floorMirrorCamera.renderTarget } );
		var floorMaterial = new THREE.MeshPhongMaterial();
		floorMaterial.color = 0xffffff;
		floor = new THREE.Mesh(floorGeometry, floorMirrorMaterial);
		floor.rotation.x = Math.PI/2;
		floor.position.y = -0.5;
		
		floorMirrorCamera.position = floor.position;


		/*
		*	Light Objects Here
		*/
		pointLight = new THREE.PointLight( 0xFFFFFF, 0.4, 2000 );
		pointLight.position.set(50, 20, 100);

		ambientLight = new THREE.AmbientLight( THREE.Vector3( 0.1, 0.1, 0.1 ) );

		/*
		*	Scene Stuff Here
		*/
		scene = new THREE.Scene();
		scene.add(floorMirrorCamera);
		scene.add(floor);
		scene.add(sphereMesh);
		scene.add(pointLight);
		scene.add(ambientLight);
		

		console.log(floorMirrorCamera);
		console.log(floor);
		

		/*
		*	Camera Stuff Here
		*/
		camera = new THREE.PerspectiveCamera( 30, window.innerWidth / window.innerHeight, 0.1, 10000 );
		camera.position.z = distance;

		/*
		*	Renderer Stuff Here
		*/
		renderer = new THREE.WebGLRenderer({antialias:true});
		renderer.setSize( window.innerWidth, window.innerHeight );

		/*
		*	Document Stuff Here
		*/
		container.appendChild( renderer.domElement );

		container.addEventListener('mousedown', onMouseDown, false);
		container.addEventListener('mousemove', onMouseMove, false);
		container.addEventListener('mouseup', onMouseUp, false);
	    container.addEventListener('mousewheel', onMouseWheel, false);
	    document.addEventListener('keydown', onDocumentKeyDown, false);
	    window.addEventListener('resize', onWindowResize, false);
	}

	/*
	*	Called on window resize
	*/
	function onWindowResize(event){

	}

	/*
	*	Called on keyboard input
	*/
	function onDocumentKeyDown(event){

	}

	/*
	*	Called on mouse click
	*/
	function onMouseDown(event) {
	    event.preventDefault();

	    if(event.button === 0)
			mouseClick = true;
	}

	/*
	*	Called on mouse movement
	*/
	function onMouseMove(event) {
		if(mouseClick === true){
			pointLight.position.x = event.x-650;
			pointLight.position.y = -event.y+300;
		}
	}

	/*
	*	Called on mouse click release
	*/
	function onMouseUp(event) {
		if(event.button === 1 && ambientLight.visible === true){
			ambientLight.visible = false;
		} else if (event.button === 1 && ambientLight.visible === false){
			ambientLight.visible = true;
		} else if (event.button === 0){
			pointLight.position.x = event.x-650;
			pointLight.position.y = -event.y+315;

			console.log(event.x, event.y);
			console.log(pointLight.position);
		}
		mouseClick = false;
	}

	/*
	*	Called when mouse exits an area
	*/
	function onMouseOut(event) {

	}

	/*
	*	Called on mouse wheel movement
	*/
	function onMouseWheel(event) {
		event.preventDefault();


		var delta = 0;

		if ( event.wheelDelta ) { // WebKit / Opera / Explorer 9

			delta = event.wheelDelta;


		} else if ( event.detail ) { // Firefox

			delta = - event.detail;

		}

		if(delta > 0){
			console.log("Up");
			pointLight.intensity -= 0.02;
		} else if (delta < 0){
			console.log("Down");
			pointLight.intensity += 0.02;
		}

		console.log(pointLight.intensity);
	}

	/*
	*	Animation call function. Do all animation calls here.
	*/
	function animate(){
		sphereMesh.rotation.y += 0.005;

		requestAnimationFrame(animate);
		render();
	}

	/*
	*	This is the render function. This is where you should do camera updates and any updates to the scene.
	*	
	*	It also contains the render function which draws to the screen.
	*/
	function render(){
		floor.visible = false;
		floorMirrorCamera.updateCubeMap (renderer, scene);
		floor.visible = true;

		renderer.render(scene, camera);
	}

	init();
	this.animate = animate;
	this.renderer = renderer;
	this.scene = scene;

	return this;
}